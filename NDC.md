# Note de clarification

Listes des objets à gérer dans la base de données :
-	Ressource
-	Contributeur
-	Exemplaire
-	Livre
-	Film
-	Œuvre musicale
-	Prêt 
-	Personnel
-	Adhérent
-	Suspendu

Liste des propriétés associées à chaque objet :
-	Ressource : code, titre, date d’apparition, éditeur, genre, code de classification
-	Exemplaire: id, disponibilité, état
-	Contributeur : nom, prénom, date de naissance, nationalité
-	Livre : auteur=contributeur, ISBN, résumé, langue
-	Film : réalisateur  =contributeur, acteurs=contributeur langue, synopsis, longueur
-	Œuvre musicale : compositeur=contributeur, interprete=contributeur, longueur
-	Prêt : exemplaire, adhérent, date, durée, rendu
-	Personnel : login, mdp, nom, prénom, adresse, adresse mail
-	Adhérent : login, mdp, nom, prénom, id_carte, date de naissance, adresse, adresse mail, numero de téléphone, en_cours, date dernière adhésion, blacklist
-	Suspendu : loginadhérent, datedébut, datedefin, indetermine, idpret 


Liste des contraintes sur les objets et propriétés :
-   Un adhérent ne peut pas adhérer si il est déjà adhérent pour cette année
-   Toutes les ressources existent en au moins un exemplaire
-   Une ressource ne peut avoir qu'un seul genre
-   Un livre est en une seule langue
-	Le code d'une ressource est unique
-   L'ISBN d'un livre est unique
-   Une ressource est forcément un livre, un film ou un livre.
-   Une ressource ne peut pas être de deux types différents.
-   Un livre à au moins un auteur
-   Un film à au moins un réalisateur et un acteur.
-   Une Oeuvre Musicale a au moins un compositeur et un interprete.
-   Un livre perdu n'est plus disponible
-	La bibliotheque souhaite garder une trace de toutes les adhesions, actuelles et passées donc il faudra jamais effacer une adhesion
-	Une personne doit être actuellement adhérent pour pouvoir emprunter
-   Un adhérent peut emprunteur seulement si il n'est pas blacklister.
-	Un adhérent ne peut  effectuer qu'un nombre limité d'emprunts simultanés (fixé à 5 dans notre exercice)
-	Un adhérent ne peut emprunter que si il n'est pas suspendu
-	Une adhésion expire un an après le début de celle-ci
-	La durée d'un emprunt est de 15 jours par default et peut être modifié.
-	Un document ne peut être emprunter que si il est en bon état ou neuf et qu'il est disponible.
-   Un document emprunter n'est pas disponible.
-	Si un adhérent rend un livre en retard de X jours alors il est suspendu X jours.
-   une personne doit s'authentifier avant de pouvoir effectuer des actions
-   Un contributeur doit contribuer à au moins une oeuvre
-   Si un utilisateur rend plusieurs documents en retard, on ne somme pas les sanctions, on prend seulement la plus grande.


Liste des utilisateurs appelés à modifier et consulter les données :
-	Personnel
-	Adhérent

Liste des fonctions que ces utilisateurs pourront réaliser :
-	Personnel : consulter, modifier, ajouter, s'authentifier
-	Adhérent : consulter, emprunter, rendre, rembourser, adhérer,s'authentifier

## Transformation en MLD

### L'héritage Ressource

Nous avons choisi l'héritage par Référence pour plusieurs raisons : 
-   La relation de composition entre Ressource et Exemplaire nous oblige à avoir la clé de Ressource dans Exemplaire ce qui aurait été très contraignants si nous avions réalisé une transformation par classe fille.
-   Les associations, joue/réalise; écrit, interprete/compose, aurait été très contraignantes si nous avions choisi la transformation par classe mère, car certaines ressource aurait du avoir certaines relations et pas d'autres.

Ainsi la transformation par Référence c'est imposé car elle était la moins contraignantes, il faut seulement s'assurer que toutes les Ressources sont d'un type et que une ressource n'est pas de deux types différents.

### L'héritage Personne

Nous avons ici choisi l'héritage par classe fille car nous avions pas d'associations avec la Classe Mère qui de plus est abstraite.
Les classes filles ont beaucoup d'arguments spécifique ce qui permet d’emblée d'éliminer un héritage par classe mère.
De plus, le nombre de contraintes complexes est plus faible par transformation par classes filles que par références, L'héritage par référence nous rajoutais une table et d'un point de vue métier, l'héritage par classe fille nous permet d'avoir une vraie séparation entre le personnel qui possède beaucoup plus de droit que les adhérents.

### Les contraites

-    Le login de Personnel et Adhérent est unique
vDoubleLogin = Intersection(Projection(Personnel, login), Projection(Adherent, login)) = {} 

-    Aucune ressource ne peut être un livre et un film:
vRessourceLivreEtFilm = INTERSECTION (PROJECTION(Livre,code), PROJECTION(Film,code)) = {} 

-    Aucune ressource ne peut être un livre et une oeuvre musicale:
vRessourceLivreEtOeuvreMusicale = INTERSECTION (PROJECTION(Livre,code), PROJECTION(Oeuvre_Musicale,code)) = {}

-    Aucune ressource ne peut être un livre et une oeuvre musicale:
vRessourceFilmEtOeuvreMusicale = INTERSECTION (PROJECTION(Film,code), PROJECTION(Oeuvre_Musicale,code)) = {} 

-    Toutes les ressources sont d'au moins un type
vRessourceSansType = Différence(Projection(Union(Film, Oeuvre_Musicale, Film), code), Projection(Ressource, code))={}

- Tous les contributeurs doivent contribuer à au moins une ressource
vContributeurSansRessources = Difference(Projection(Contributeur, id), Projection(Union(Realise, Interprete, Joue, Compose, Ecrit), id)) = {} 

-   Un livre à au moins un auteur
vLivreSansAuteur = Différence(Projection(Livre, Code), Projection(Ecrit,Code)) ={}

-   Un film à au moins un réalisateur et un acteur.
vFilmSansRealisteur =  Différence(Projection(Film, Code), Projection(Realise,Code)) ={}

vFilmSansActeur =  Différence(Projection(Film, Code), Projection(Joue,Code)) ={}

-   Une Oeuvre Musicale a au moins un compositeur et un interprete.
vOeuvreMusicaleSansCompositeur =  Différence(Projection(Oeuvre_Musicale, Code), Projection(Compose,Code)) ={}

vOeuvreMusicaleSansInterprete =  Différence(Projection(Oeuvre_Musicale, Code), Projection(Interprete,Code)) ={}

vRessourceSansContributeur = Union(vOeuvreMusicaleSansInterprete, vOeuvreMusicaleSansCompositeur,vFilmSansActeur,vFilmSansRealisteur,vLivreSansAuteur)

-   Toutes les ressources existent en au moins un exemplaire
VRessourcesSansExemplaire = Différence(Projetion(Ressource, code), Projection(Exemplaire,code) = 0

-	Un document ne peut être emprunté que s'il est neuf ou bon état.
vEtatemprunt = Intersection( Projection(Restriction(Pret, rendu = false), code, id), Projection(Restriction(Exemplaire, etat = abimé ou etat = perdu), code, id)) = {}

-   Un livre perdu n'est plus disponible
vLivrePerduEtDisponible = Intersection(Projection(Restriction(Exemplaire, etat = perdu), id, code), Projection(Restriction(Exemplaire,  disponibilite = True), id, code)) = {}


-   Un exemplaire emprunter n'est pas disponible.
vExemplaireEmprunteEtDisponible = Intersection(Projection(Restriction(Exemplaire, Disponibilite = True), id, code), Projection(Restriction(Pret, rendu = False), id, code)) = {}


- Un adhérent suspendu est soit suspendu pour retard soit pour une attente de remboursement.
Projection(Restriction(Suspendu, termine = false), id) = Union(Projection(Restriction(Suspendu, datefin > datedujour ET termine = False), id), Projection(Restriction(Suspendu, attente_remboursement = true ET termine = False), id))

- Un adhérent ne peut emprunter que si il n'est pas suspendu
vSuspendu = Intersection( Projection(Restriction(Pret, rendu = false), login), Projection(Union(Restriction(Suspendu, datefin > datedujour), Restriction(Suspendu, attente_remboursement = true)), login) = {}

- Un adhérent ne peut emprunter que si il n'est pas blacklisté
vBlacklister = Interserction(Projection(Restriction(Pret, rendu = false), login), Projection(Restriction(adherent, blacklist = true), login)) = {}

- Une personne ne peut emprunter que si elle est adhérente
vNonAdherentAvecEmprunt = Intersection(Projection(Restriction(Adhesion, datefin > date_du_jour), login), Projection(Restriction(Emprunt, rendu = false), login)) = {}

Contraintes qui ne peuvent pas être traduite en MLD : 

- Un adhérent ne peut emprunter que 5 exemplaires simultanément
-	Si un adhérent rend un livre en retard de X jours alors il est suspendu X jours.
-	Un adhérent ne peut pas adhérer si il est déjà adhérent pour cette année
-	La durée d'un emprunt est de 15 jours par default
-	Un document ne peut être emprunter que si il est disponible

## Normalisation

3NF ssi toutes tables est en 3NF :

 - Personnel :
clé candidate : login
1NF : 
	1 clé : login
	attribut atomique
2NF : 
	Une unique clé candidate atomique
3NF :
	Tout attribut n'est déterminé que par login

 - Adherent :
clé candidate : login, id_carte
1NF : 
	1 clé : login
	attribut atomique
2NF : 
	clés candidates atomiques
3NF :
	Seuls les clés candidates déterminent d'autres attributs

 - Adhesion :
clé candidate : (login,datefin)
1NF : 
	1 clé : (login,datefin)
	attributs atomiques
2NF : 
	pas d'attribut nom clé
3NF :
	pas d'attribut non-clé

 - Contributeur :
clé candidate : id
1NF : 
	1 clé : id
	attributs atomiques
2NF : 
	Une unique clé candidate atomique
3NF :
	Tout attribut autre que id n'est déterminé que par id

 - Ressources :
clés candidates code
1NF : 
	1 clé : code
	attribut atomique
2NF : 
	clés candidates atomiques
3NF :
	Tout attribut autre que code n'est déterminé que par code
    
 - Livre :
clés candidates code, ISBN
1NF : 
	1 clé : code
	attribut atomique
2NF : 
	clés candidates atomiques
3NF :
	Tout attribut n'est déterminé que par code ou ISBN

 - Film :
clé candidate code
1NF : 
	1 clé : code
	attribut atomique
2NF : 
	Une unique clé candidate atomique
3NF :
	Tout attribut autre que code n'est déterminé que par code

 - Oeuvre_musical:
clé candidate : code
1NF : 
	1 clé : code
	attribut atomique
2NF : 
	Une unique clé candidate atomique
3NF :
	Tout attribut n'est déterminé que par code

 - Ecrit :
clé candidate : (id,code)
1NF : 
	1 clé : (id,code)
	attribut atomique
2NF : 
	pas d'attribut nom clé
3NF :
	pas d'attribut nom clé

 - Joue :
clé candidate : (id,code)
1NF : 
	1 clé : (id,code)
	attribut atomique
2NF : 
	pas d'attribut nom clé
3NF :
	pas d'attribut nom clé


 - Realise :
clé candidate : (id,code)
1NF : 
	1 clé : (id,code)
	attribut atomique
2NF : 
	pas d'attribut nom clé
3NF :
	pas d'attribut nom clé


 - Interprete :
clé candidate : (id,code)
1NF : 
	1 clé : (id,code)
	attribut atomique
2NF : 
	pas d'attribut nom clé
3NF :
	pas d'attribut nom clé

 - Compose :
clé candidate : (id,code)
1NF : 
	1 clé : (id,code)
	attribut atomique
2NF : 
	pas d'attribut nom clé
3NF :
	pas d'attribut nom clé

- Exemplaire :
clé candidate : (id,code)
1NF : 
	1 clé : (id,code)
	attribut atomique
2NF : 
	Auncun attribut autre que id ou code n'est déterminé que par id seul ou code seul
3NF :
	Tout attribut n'est déterminé que par (id,code)

- Pret :
clé candidate : id, (code, exemplaire, date_emprunt)
1NF : 
	1 clé : id
	attribut atomique
2NF : 
	Auncun attribut autre que id, exemplaire ou date_emprunt n'est déterminé que par id seul, date_emprunt seul ou exemplaire seul
3NF :
	Tout attribut n'est déterminé que par id ou (code, exemplaire, date_emprunt)

- Suspendu :
clé candidate : id
1NF : 
	1 clé : id
	attribut atomique
2NF : 
	Une unique clé candidate atomique
3NF :
	Tout attribut n'est déterminé que par id

Remarque: si l'attribut login des tables Personnel et Adherent n'est pas généré à partir d'autres attributs (exemple login = nomprenom) - et de façon plus générale aucune clé artificielle (id, code etc.) n'est élaborée à partir d'autres attributs - alors le système est en BCNF.