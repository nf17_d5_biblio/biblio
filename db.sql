CREATE TABLE Personnel(
    login VARCHAR(25) PRIMARY KEY,
    mdp VARCHAR(50) NOT NULL,
    nom CHAR(50) NOT NULL,
    prenom CHAR(50) NOT NULL,
    adresse VARCHAR(100) NOT NULL,
    adresse_mail VARCHAR(100) NOT NULL CHECK (adresse_mail LIKE '%@%') 
);

CREATE TABLE Adherent (
     login VARCHAR(25) PRIMARY KEY,
     mdp VARCHAR(50)  NOT NULL,
     nom VARCHAR(50)  NOT NULL,
     prenom VARCHAR(50)  NOT NULL,
     adresse_mail VARCHAR(100) CHECK (adresse_mail LIKE '%@%'),
     adresse VARCHAR(100)  NOT NULL,
     id_carte INTEGER  NOT NULL UNIQUE, 
     date_naissance DATE  NOT NULL CHECK (date_naissance < current_date), 
     numero_tel VARCHAR(10) NOT NULL, 
     blacklist BOOLEAN DEFAULT False
);


CREATE TABLE Adhesion(
    login VARCHAR(25) NOT NULL,
    datefin DATE DEFAULT  current_date + interval '1 year' CHECK (datefin > current_date),
    FOREIGN KEY (login) REFERENCES Adherent(login),
    PRIMARY KEY (login, datefin)
);

CREATE TABLE Contributeur(
    id SERIAL PRIMARY KEY, 
    nom VARCHAR(50) NOT NULL, 
    prenom VARCHAR(50) NOT NULL, 
    date_naissance DATE NOT NULL CHECK (date_naissance < current_date), 
    nationalité VARCHAR(50) NOT NULL
);
 
CREATE TABLE Ressource(
    code VARCHAR(25) PRIMARY KEY, 
    titre VARCHAR(80)NOT NULL, 
    date_apparition DATE NOT NULL, 
    editeur VARCHAR(25) NOT NULL, 
    genre VARCHAR(25) NOT NULL, 
    code_classification VARCHAR(25) NOT NULL 
);


CREATE TABLE Livre(
    code VARCHAR(25) PRIMARY KEY,
    langue VARCHAR(25) NOT NULL,
    ISBN BIGINT NOT NULL UNIQUE, 
    resume TEXT,
    FOREIGN KEY (code) REFERENCES Ressource(code)
);

CREATE TABLE Film(
    code VARCHAR(25) PRIMARY KEY,  
    langue VARCHAR(25) NOT NULL, 
    synopsis TEXT NOT NULL, 
    longueur TIME NOT NULL,
    FOREIGN KEY (code) REFERENCES Ressource(code)
);

CREATE TABLE Oeuvre_Musicale(
    code VARCHAR(25) PRIMARY KEY, 
    longueur TIME NOT NULL,
    FOREIGN KEY (code) REFERENCES Ressource(code)
);



CREATE TABLE Ecrit(
id INTEGER NOT NULL, 
code VARCHAR(25) NOT NULL,
PRIMARY KEY (id, code),
FOREIGN KEY (id) REFERENCES Contributeur(id),
FOREIGN KEY (code) REFERENCES Livre(code)
);

CREATE TABLE Joue(
id INTEGER NOT NULL, 
code VARCHAR(25) NOT NULL,
PRIMARY KEY (id, code),
FOREIGN KEY (id) REFERENCES Contributeur(id),
FOREIGN KEY (code) REFERENCES Film(code)
);

CREATE TABLE Realise(
id INTEGER NOT NULL, 
code VARCHAR(25) NOT NULL,
PRIMARY KEY (id, code),
FOREIGN KEY (id) REFERENCES Contributeur(id),
FOREIGN KEY (code) REFERENCES Film(code)
);

CREATE TABLE Interprete(
id INTEGER NOT NULL, 
code VARCHAR(25) NOT NULL,
PRIMARY KEY (id, code),
FOREIGN KEY (id) REFERENCES Contributeur(id),
FOREIGN KEY (code) REFERENCES Oeuvre_Musicale(code)
);

CREATE TABLE Compose(
id INTEGER NOT NULL, 
code VARCHAR(25) NOT NULL,
PRIMARY KEY (id, code),
FOREIGN KEY (id) REFERENCES Contributeur(id),
FOREIGN KEY (code) REFERENCES Oeuvre_Musicale(code)
);


CREATE TYPE ETAT AS ENUM ('neuf','bon','abime','perdu');

CREATE TABLE Exemplaire(
id INTEGER NOT NULL,
code VARCHAR(25) NOT NULL,
disponibilite BOOLEAN DEFAULT true,
prix MONEY,
etat ETAT DEFAULT 'neuf',
FOREIGN KEY (code) REFERENCES Ressource(code),
PRIMARY KEY (id, code)
);


CREATE TABLE Pret(
id SERIAL PRIMARY KEY, 
login VARCHAR(25) NOT NULL,
exemplaire INTEGER NOT NULL,
code VARCHAR(25) NOT NULL,
date_emprunt DATE DEFAULT current_date,
duree INTEGER DEFAULT 15,
rendu BOOLEAN DEFAULT false,
FOREIGN KEY (login) REFERENCES Adherent(login), 
FOREIGN KEY (exemplaire, code) REFERENCES Exemplaire(id, code)
);

CREATE TABLE Suspendu(
id INTEGER PRIMARY KEY,
datedebut DATE DEFAULT current_date,
datefin DATE,
attente_remboursement BOOLEAN,
termine BOOLEAN DEFAULT False,
FOREIGN KEY (id) REFERENCES Pret(id),
CHECK ((Datefin > current_date) OR (attente_remboursement = true))
);






--JEU DE DONNEES 










--PERSONNEL

INSERT INTO Personnel VALUES('cbrigitte', 'cestmoilaplusbelle','DE LA COMPTA', 'Brigitte', '52 rue du grand férée 60200 Compiègne', 'brigittedelacompta@glmail.fr');

INSERT INTO Personnel VALUES('jmarot', 'jvdkuh56', 'Marot', 'Jacques', '44 rue du peuplier 45300 Bourguy', 'jacques.marot@gmail.fr');

INSERT INTO Personnel VALUES('sfreud', 'lereve123','Freud', 'Sigmund', '6 rue des épiciers 60200 Copiègne', 'sisifreud@glmail.com');

INSERT INTO Personnel VALUES('emusk', 'tesla098', 'Musk', 'Elon', '12 rue des astronautes 95000 Cergy', 'elon.musk@gmail.fr');

--ADHERENT
INSERT INTO Adherent VALUES('jbon', 'dsvjbdsk', 'Bon', 'Jean', 'jeanbon@free.fr','56 place de la victoire 45260 Bouzigue', 17354, '26-03-1964', '0653427163', false);

INSERT INTO Adherent VALUES('jbarthel','12345cestsecurise', 'BARTHELEMY', 'Jean-Gabriel', 'jbartehl@etu.utc.fr', '2 avenue Stephane Crozat 60200 Compiegne', 174, '2000-06-08', '0782800514');
INSERT INTO Adhesion VALUES('jbarthel');

INSERT INTO Adherent VALUES('alancele', '0000', 'Lanceleur', 'Anne', 'anne.lanceleur@etu.utc.fr', '21 rue des Données 60200 Compiegne', 4658, '22-01-2000', '0763421083', false);

INSERT INTO Adhesion VALUES ('alancele');

INSERT INTO Adherent VALUES('yassila', 'mdpamoi', 'Assila', 'Yassine', 'yassine.assila@etu.utc.fr', '19 rue des Marmottes 60200 Compiegne', 4589, '27-10-1995', '0657453726', true);

INSERT INTO Adhesion VALUES ('yassila');

INSERT INTO Adherent VALUES('emacron', 'cestnotreprojet', 'Macron', 'Emmanuel', 'manu.macron@hotmail.com', '12 rue des abeilles 77000 Melun', 0892, '26-04-1981', '0698732732', false);

INSERT INTO Adherent VALUES('jesuisbloque', 'jfeh150', 'Faitkamatete', 'Jean', 'jesuisunimbecile@free.fr','56 ruelle des idiots 75000 Paris', 17355, '26-03-1000', '0657827163', True);
INSERT INTO Adhesion VALUES ('jesuisbloque');

--RESSOURCE

INSERT INTO Ressource VALUES('qhr2', 'Titanic', '25-01-1995', 'Fox', 'romance', 263);

--INSERT INTO Ressource VALUES('khf1', 'Sevens souls', '15-03-2002', '21 Century', 'Dramatique', 124);

--INSERT INTO Ressource VALUES('sdb3', 'Vous pouvez embrasser le marié', '29-08-2019', 'Pathé', 'Comedie', 879);

INSERT INTO Ressource VALUES('vsh2', 'Libertango', '27-12-1981', 'Musix', 'contemporain', 563);

INSERT INTO Ressource VALUES('iyr6', 'Boogie Wanderland', '05-03-1979', 'Jungle', 'funk', 983);

INSERT INTO Ressource VALUES('hfj2', 'Les Misérables', '07-03-1862', 'Gallimard', 'roman', 723);

INSERT INTO Ressource VALUES('Fsda1', E'La Communauté de l\'anneau', '25-01-2001', 'Metropolitan Filmexport', 'Science-Fiction', 'Fsda1');

INSERT INTO Ressource VALUES('Fsda2', 'Les Deux Tours', '23-02-2002', 'New Line Cinema', 'Fantasy', 394);

INSERT INTO Ressource VALUES('Lind', E'Indomptable : Le secret de l\'ame Masculine', '1-10-2005', 'Farel éditions', 'Témoignage', 'Lind');
INSERT INTO Ressource VALUES('Lcdf', 'Coeur de femme', '17-10-2007', 'Farel éditions', 'Témoignage', 'Lcdf');

--Livres

INSERT INTO Livre VALUES('hfj2', 'français', 1846300000, 'Histoire de Jean Valjean de sa sortie du bagne à sa mort dans le Paris du XIXe');

--INSERT INTO Livre VALUES('khf1', 'anglais', 9781558184695, 'Après avoir provoqué un accident de voiture qui a couté la vie de sept personnes Jason cherche sa redemption');

INSERT INTO Livre VALUES('Lind', 'anglais', 9782863142738, E'La masculinité et la virilité. Deux concepts aujourd’hui bien peu en vogue, deux réalités en souffrance chez beaucoup d’hommes qui peinent à trouver leur place, dans leur couple, leur famille ou la société. Pour beaucoup, le prix est cher à payer : faiblesses d’un corps mal dompté, fuite dans des compensations éphémères, enfermement dans des prisons virtuelles, machisme exacerbé pour mieux cacher ses doutes et ses fragilités… Au final, dégoût et ennui profond d’une vie qui ne comble pas, tant elle semble « petite » et « ordinaire ».');
INSERT INTO Livre VALUES('Lcdf', 'anglais', 9782863143490, E'Soyez tranquille ! Ce livre ne traite pas de ce que vous devez ou ne devez pas faire en tant que femme. Nous sommes lasses de ces ouvrages... Leurs messages semblent dire : « Tu n\'es pas la femme que tu devrais être, mais si tu suis les dix étapes proposées, tu peux encore réussir ta vie. » Dans leur grande majorité, ces livres assassinent l\'ame. Car la féminité ne se réduit pas à une formule.
Nous avons des amies qui aiment les moments de détente autour d\'une tasse de thé en porcelaine, et d\'autres qui frémissent d\'horreur à cette pensée... Alors qui représente la vraie femme : Cendrillon, Jeanne d\'Arc ou Marie-Madeleine ? Comment assumer sa vraie féminité sans tomber dans des stéréotypes, ou pire, générer davantage de pression ou d\'humiliation ? C\'est bien la dernière chose qu\'il nous faut ! .');
--Films

INSERT INTO Film VALUES('qhr2', 'anglais', 'Belle histoire', '03:00:00');

--INSERT INTO Film VALUES('sdb3', 'français', 'Histoire d amour', '01:59:00');

INSERT INTO Film VALUES('Fsda1', 'Anglais', E'Sur la Terre du Milieu, dans la paisible région de la Comté, vit le Hobbit Frodon Sacquet. Comme tous les Hobbits, Frodon est un bon vivant, amoureux de la terre bien cultivée et de la bonne chère. Orphelin alors qu''il n''était qu''un enfant, il s''est installé à Cul-de-Sac chez son oncle Bilbon, connu de toute la Comté pour les aventures extraordinaires qu''il a vécues étant jeune et les trésors qu''il en a tirés. Le jour de ses 111 ans, Bilbon donne une fête grandiose à laquelle est convié le puissant magicien Gandalf le Gris. C''est en ce jour particulier que Bilbon décide de se retirer chez les Elfes pour y finir sa vie. Il laisse en héritage à Frodon son trou de Hobbit ainsi qu''un anneau, qu''il a autrefois trouvé dans la caverne d''une créature nommée Gollum dans les Monts Brumeux, et qui a le pouvoir de rendre invisible quiconque le porte à son doigt', '2:58:00');

INSERT INTO Film VALUES('Fsda2', 'Anglais', E'Après être entrés en Emyn Muil, Frodon Sacquet et Sam Gamegie rencontrent la créature Gollum, qui essaye de leur voler l''Anneau par la force. Vaincu, il promet aux Hobbits de les guider jusqu''au Mordor. Après avoir traversé l''Emyn Muil et les marais des Morts, ils arrivent à la Morannon, la « Porte Noire » de Mordor. Cependant, elle est trop bien protégée pour qu''ils entrent par là et Gollum leur propose de leur montrer le chemin secret de Cirith Ungol. Pendant le voyage, ils rencontrent une troupe avancée du Gondor, dirigée par Faramir, fils de l''Intendant Denethor II et frère de Boromir. Il les fait prisonniers et découvre qu''ils portent l''Anneau unique. Il décide alors de les mener devant son père, mais, en traversant la cité détruite d''Osgiliath, les soldats du Gondor sont confrontés aux forces de Sauron menées par des Nazgûl. Se rendant compte du pouvoir maléfique de l''Anneau sur Frodon, qui a presque été pris par un des Nazgûl, Faramir se résout à les libérer pour qu''ils accomplissent leur mission. ', '3:45:00');

--Oeuvres Musicales

INSERT INTO Oeuvre_Musicale VALUES('vsh2', '00:05:30');

INSERT INTO Oeuvre_Musicale VALUES('iyr6', '00:04:37');

-- Exemplaires
INSERT INTO Exemplaire VALUES(1, 'Fsda1');
INSERT INTO Exemplaire VALUES(2, 'Fsda1', default, 15.3, 'bon');
INSERT INTO Exemplaire VALUES(3, 'Fsda1', false, 15.3, 'perdu');
INSERT INTO Exemplaire VALUES(1, 'Fsda2', true, 16.26, 'bon');

INSERT INTO Exemplaire VALUES(1, 'qhr2', true, 15, 'bon');

INSERT INTO Exemplaire VALUES(1, 'vsh2', true, 9.99, 'neuf');

INSERT INTO Exemplaire VALUES(1, 'hfj2', true, 7.50, 'abime');

INSERT INTO Exemplaire VALUES(2, 'hfj2', false, 7.50, 'neuf');

INSERT INTO Exemplaire VALUES(3, 'hfj2', true, 7.50, 'bon');

--INSERT INTO Exemplaire VALUES(1, 'khf1', true, 8.70, 'neuf');

--INSERT INTO Exemplaire VALUES(2, 'khf1', false, 8.70, 'bon');

INSERT INTO Exemplaire VALUES(1, 'iyr6', true, 6.40, 'abime');

INSERT INTO Exemplaire VALUES(2, 'iyr6', false, 6.40, 'perdu');

--INSERT INTO Exemplaire VALUES(1, 'sdb3', true, 12.30, 'neuf');

--INSERT INTO Exemplaire VALUES(2, 'sdb3', false, 12.30, 'bon');
INSERT INTO Exemplaire VALUES(1, 'Lind');
INSERT INTO Exemplaire VALUES(1, 'Lcdf');

--Contributeur
INSERT INTO Contributeur VALUES(default, 'Peter', 'Jackson', '31-10-1961', 'Nouvelle-Zélande');
INSERT INTO Contributeur VALUES(default, 'Wood', 'Elijah', '28-01-1981', 'USA');

INSERT INTO Contributeur VALUES(default, 'White','Maurice', '01-01-1956', 'Etats-Unis');
--INSERT INTO Contributeur VALUES(default, 'Jhon','Doe', '14-04-1989', 'Etats-Unis');

--INSERT INTO Contributeur VALUES(default, 'Jhon','Doe', '14-04-1989', 'Etats-Unis');

INSERT INTO Contributeur VALUES(default, 'McKellen','Ian', '25-05-1939', 'Angleterre');

INSERT INTO Contributeur VALUES(default, 'Eldredge','John', '25-11-1960', 'USA');
INSERT INTO Contributeur VALUES(default, 'Eldredge','Stesi', '14-11-1960', 'USA');

INSERT INTO Contributeur VALUES(default, 'Cameron', 'James', '16-08-1954', 'Canada');
INSERT INTO Contributeur VALUES(default, 'DiCaprio', 'Leonardo', '11-11-1974', 'USA');
INSERT INTO Contributeur VALUES(default, 'Winslet', 'Kate', '5-10-1975', 'Angleterre');

INSERT INTO Contributeur VALUES(default, 'Piazzolla', 'Astor', '11-03-1921', 'Argentine');

INSERT INTO Contributeur VALUES(default, 'Hugo', 'Victor', '26-02-1802', 'France');

-- Ecrit
INSERT INTO Ecrit VALUES(5, 'Lind');
INSERT INTO Ecrit VALUES(5, 'Lcdf');
INSERT INTO Ecrit VALUES(6, 'Lcdf');
INSERT INTO Ecrit VALUES(11, 'hfj2');

--Joue
INSERT INTO Joue VALUES(2, 'Fsda1');
INSERT INTO Joue VALUES(2, 'Fsda2');
INSERT INTO Joue VALUES(4, 'Fsda1');
INSERT INTO Joue VALUES(4, 'Fsda2');
INSERT INTO Joue VALUES(8, 'qhr2');
INSERT INTO Joue VALUES(9, 'qhr2');

--Réalise
INSERT INTO Realise VALUES(1, 'Fsda1');
INSERT INTO Realise VALUES(1, 'Fsda2');
INSERT INTO Realise VALUES(3, 'Fsda2');
INSERT INTO Realise VALUES(7, 'qhr2');

--Interprete

INSERT INTO Interprete VALUES(3, 'iyr6');
INSERT INTO Interprete VALUES(10, 'vsh2');


--Compose
INSERT INTO Compose VALUES(1, 'iyr6');
INSERT INTO Compose VALUES(10, 'vsh2');

--Pret

INSERT INTO Pret VALUES(default, 'alancele', 1, 'qhr2', default, default, default);
UPDATE Exemplaire SET disponibilite = false WHERE (Exemplaire.id = 1 AND Exemplaire.code = 'qhr2');
INSERT INTO Pret VALUES(default, 'jbarthel', 1, 'Fsda1', default, default, default);
UPDATE Exemplaire SET disponibilite = false WHERE (Exemplaire.id = 1 AND Exemplaire.code = 'Fsda1');
INSERT INTO Pret VALUES(default, 'jbarthel', 1, 'iyr6', default, default, True);
INSERT INTO Pret VALUES(default, 'jesuisbloque', 2, 'hfj2', '01-04-2020', default, True);
INSERT INTO Pret VALUES(default, 'emacron', 1, 'Lcdf', '20-12-2019', default, True);

--Suspendu
INSERT INTO Suspendu VALUES(4, Default, '15-4-2020', True, False);








--VUES








CREATE VIEW VLivres AS
SELECT Ressource.code, titre, date_apparition, editeur, genre, code_classification, ISBN, resume, langue
FROM Ressource, Livre
WHERE Ressource.code=Livre.code;

CREATE VIEW vFilms AS
SELECT Ressource.code, titre, date_apparition, editeur, genre, code_classification, langue, synopsis, longueur
FROM Ressource, Film
WHERE Ressource.code=Film.code;

CREATE VIEW vOM AS
SELECT Ressource.code, titre, date_apparition, editeur, genre, code_classification, longueur
FROM Ressource, Oeuvre_Musicale
WHERE Ressource.code=Oeuvre_Musicale.code;

CREATE VIEW vDoubleLogin AS
SELECT login From Personnel
INTERSECT
SELECT login FROM Adherent;

CREATE VIEW vRessourceLivreEtFilm AS
SELECT code FROM Livre
INTERSECT
SELECT code FROM Film;

CREATE VIEW vRessourceLivreEtOeuvreMusicale AS 
SELECT code FROM Livre
INTERSECT
SELECT code FROM Oeuvre_Musicale;

CREATE VIEW vRessourceFilmEtOeuvreMusicale AS
SELECT code FROM Oeuvre_Musicale
INTERSECT
SELECT code FROM Film;

CREATE VIEW vRessourceSansType AS
SELECT code FROM Ressource
EXCEPT
(SELECT code FROM Film
UNION SELECT code FROM Oeuvre_musicale
UNION SELECT code FROM Livre);

CREATE VIEW vContributeurSansRessources AS
SELECT id FROM Contributeur
EXCEPT
(SELECT id FROM Realise
UNION
SELECT id FROM Joue
UNION
SELECT id FROM Compose
UNION
SELECT id FROM Interprete
UNION
SELECT id FROM Ecrit);

CREATE VIEW vLivreSansAuteur AS
SELECT Code From Livre
EXCEPT
SELECT Code From Ecrit;

CREATE VIEW vFilmSansRealisteur AS
SELECT Code From Film
EXCEPT
SELECT Code From Realise;

CREATE VIEW vFilmSansActeur AS
SELECT Code From Film
EXCEPT
SELECT Code From Joue;

CREATE VIEW vOeuvreMusicaleSansInterprete AS
SELECT Code From Oeuvre_Musicale
EXCEPT
SELECT Code From Interprete;

CREATE VIEW vOeuvreMusicaleSansCompositeur AS
SELECT Code From Oeuvre_Musicale
EXCEPT
SELECT Code From Compose;

CREATE VIEW vRessourceSansContributeur AS
SELECT * FROM vOeuvreMusicaleSansInterprete
UNION SELECT * FROM vOeuvreMusicaleSansCompositeur
UNION SELECT * FROM vFilmSansActeur
UNION SELECT * FROM vFilmSansRealisteur
UNION SELECT * FROM vLivreSansAuteur;

CREATE VIEW vRessourceSansExemplaire AS 
SELECT code FROM Ressource EXCEPT SELECT code FROM Exemplaire;

CREATE VIEW vEtatemprunt AS
SELECT Pret.login, Pret.id, Pret.exemplaire, Pret.code
FROM Pret, Exemplaire
WHERE Pret.exemplaire=Exemplaire.id AND Pret.code = Exemplaire.code AND Pret.rendu = false AND (Exemplaire.etat = 'abime' OR Exemplaire.etat = 'perdu');

CREATE VIEW vPretsParPers AS 
SELECT login, count(*)
FROM Pret
WHERE Pret.rendu = false 
GROUP BY login;

CREATE VIEW vPlusde5Emprunts AS
SELECT login, count(*)
FROM Pret
WHERE Pret.rendu = false
GROUP BY login
HAVING count(*) > 5;

CREATE VIEW vLivrePerduEtDisponible AS 
SELECT id, code FROM EXemplaire WHERE disponibilite = True AND etat = 'perdu';

CREATE VIEW vExemplaireEmprunteEtDisponible  AS
SELECT exemplaire, code FROM Pret WHERE Rendu = False
INTERSECT
SELECT id as exemplaire, code FROM Exemplaire WHERE disponibilite = True;

CREATE VIEW vSuspenduAvecEmprunt AS
SELECT login from Suspendu, Pret 
WHERE ((Suspendu.id = Pret.id) AND ((datefin > current_date) OR (attente_remboursement =false)))
INTERSECT
SELECT login from Pret WHERE rendu = false;

CREATE VIEW vBlacklister AS 
SELECT login FROM Pret WHERE rendu = false
INTERSECT
SELECT login FROM Adherent WHERE blacklist = True;

CREATE VIEW vNonAdherentAvecEmprunt AS
SELECT login FROM Adhesion WHERE datefin < current_date
INTERSECT
SELECT login FROM Pret WHERE rendu = false;



