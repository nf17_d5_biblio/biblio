Tout les attributs sont NOT NULL sauf mention contraire (NULLABLE).
Les vue exprime des contraintes complexes, le lien est effectué dans la NDC.

Personnel(#login: string, mdp: string, nom: string, prenom: string, adresse: string, adresse mail: string)
Adhérent(#login: string, mdp: string, nom: string, prenom: string, adresse: string, adresse mail: string, id_carte: integer, date_naissance: date, numero_tel: integer, blacklist: boolean)
Adhésion(#login=>adhérent, #datedefin : date)

vDoubleLogin = Intersection(Projection(Personnel, login), Projection(Adherent, login)) = {} 


Ressource(#code: string, titre: string, date d'apparition: date, editeur: string, genre: string, code de classification: integer)
Livre(#code=>Ressource, ISBN: integer, resume: text, langue: string) avec ISBN clé candidate
Film(#code=>Ressource, langue: string, synopsis: text, longueur: time)
Oeuvre_Musicale(#code=>Ressource, longueur: time)

vRessourceLivreEtFilm = INTERSECTION (PROJECTION(Livre,code), PROJECTION(Film,code)) = {} 
vRessourceLivreEtOeuvreMusicale = INTERSECTION (PROJECTION(Livre,code), PROJECTION(Oeuvre_Musicale,code)) = {}
vRessourceFilmEtOeuvreMusicale = INTERSECTION (PROJECTION(Film,code), PROJECTION(Oeuvre_Musicale,code)) = {} 
vRessourceSansType = Différence(Projection(Union(Film, Oeuvre_Musicale, Film), code), Projection(Ressource, code))={}


Contributeur(#id: integer, nom: string, prenom: string, date_naissance: string, nationalité: string)
Ecrit(#id=>Contributeur, #code=>Livre)
Joue(#id=>Contributeur, #code=>Film)
Realise(#id=>Contributeur, #code=>Film)
Interprete(#id=>Contributeur, #code=>Oeuvre_Musicale)
Compose(#id=>Contributeur, #code=>Oeuvre_Musicale)

vContributeurSansRessources = Difference(Projection(Contributeur, id), Projection(Union(Realise, Interprete, Joue, Compose, Ecrit), id)) = {} 
vLivreSansAuteur = Différence(Projection(Livre, Code), Projection(Ecrit,Code)) ={}
vFilmSansRealisteur =  Différence(Projection(Film, Code), Projection(Realise,Code)) ={}
vFilmSansActeur =  Différence(Projection(Film, Code), Projection(Joue,Code)) ={}
vOeuvreMusicaleSansCompositeur =  Différence(Projection(Oeuvre_Musicale, Code), Projection(Compose,Code)) ={}
vOeuvreMusicaleSansInterprete =  Différence(Projection(Oeuvre_Musicale, Code), Projection(Interprete,Code)) ={}
vRessourceSansContributeur = Union(vOeuvreMusicaleSansInterprete, vOeuvreMusicaleSansCompositeur,vFilmSansActeur,vFilmSansRealisteur,vLivreSansAuteur)


Exemplaire(#id: integer, #code=>Ressource, disponibilite: boolean, prix: money NULLABLE, etat:{neuf, bon, abime, perdu})
Pret(#id: integer, login=>Adhérent, exemplaire=>Exemplaire, code=>Ressource, date: date, duree: integer, rendu: boolean) avec (code, exemplaire, date_emprunt) clé candidate
Suspendu(#login=>Pret, #id=>Pret, datedebut: date, datefin: date NULLABLE, attente_remboursement: boolean, termine : boolean)

VRessourcesSansExemplaire = Différence(Projetion(Ressource, code), Projection(Exemplaire,code) = 0
vEtatemprunt = Intersection( Projection(Restriction(Pret, rendu = false), code, id), Projection(Restriction(Exemplaire, etat = abimé ou etat = perdu), code, id)) = {}
vLivrePerduEtDisponible = Intersection(Projection(Restriction(Exemplaire, etat = perdu), id, code), Projection(Restriction(Exemplaire,  disponibilite = True), id, code)) = {}
vExemplaireEmprunteEtDisponible = Intersection(Projection(Restriction(Exemplaire, Disponibilite = True), id, code), Projection(Restriction(Pret, rendu = False), id, code)) = {}
Projection(Restriction(Suspendu, termine = false), id) = Union(Projection(Restriction(Suspendu, datefin > datedujour ET termine = False), id), Projection(Restriction(Suspendu, attente_remboursement = true ET termine = False), id))
vSuspendu = Intersection( Projection(Restriction(Pret, rendu = false), login), Projection(Union(Restriction(Suspendu, datefin > datedujour), Restriction(Suspendu, attente_remboursement = true)), login) = {}
vBlacklister = Interserction(Projection(Restriction(Pret, rendu = false), login), Projection(Restriction(adherent, blacklist = true), login)) = {}
vNonAdherentAvecEmprunt = Intersection(Projection(Restriction(Adhesion, datefin > date_du_jour), login), Projection(Restriction(Emprunt, rendu = false), login)) = {}


Contraintes qui ne peuvent pas être traduite en MLD : 
-   Un adhérent ne peut emprunter que 5 exemplaires simultanément (Une vue en SQL a été réalisé)
-	Si un adhérent rend un livre en retard de X jours alors il est suspendu X jours. (Réalisable seulement sur la couche applicative)
-	Un adhérent ne peut pas adhérer si il est déjà adhérent pour cette année. (Réalisable seulement sur la couche applicative)
-	La durée d'un emprunt est de 15 jours par default (Réalisé en SQL)